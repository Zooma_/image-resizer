import unittest
from app import create_app
from app.tasks import celery


class BaseTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client()
        celery.conf.update({
            'task_always_eager': True,
            'imports': ['app.tasks']
        })

    def tearDown(self) -> None:
        self.app_context.pop()
