from tests.base_test_case import BaseTestCase
from app.api import utils


class TestUtils(BaseTestCase):
    def test_size_valid(self):
        size = (1, 1)
        self.assertTrue(utils.valid_size(size))
        size = (9999, 9999)
        self.assertTrue(utils.valid_size(size))
        size = (0, 0)
        self.assertFalse(utils.valid_size(size))
        size = (10000, 10000)
        self.assertFalse(utils.valid_size(size))
        size = (1, -1)
        self.assertFalse(utils.valid_size(size))

    def test_allowed_extension(self):
        self.assertTrue(utils.allowed_extension('image/jpg'))
        self.assertTrue(utils.allowed_extension('image/jpeg'))
        self.assertTrue(utils.allowed_extension('image/png'))
        self.assertFalse(utils.allowed_extension('notimage/ext'))
