import os
from time import sleep
from tests.base_test_case import BaseTestCase
from flask import current_app
from os.path import exists


class TestApi(BaseTestCase):

    def test_api_doc(self):
        response = self.client.get('/api/')
        self.assertEqual(response.status_code, 200)

    def test_post_resize_image(self):
        with open(current_app.config['TEST_IMAGE'], 'rb') as fp:
            payload = {
                'image_data': fp,
                'size_target': '{"width": 100, "height": 100}'
            }
            response = self.client.post(
                path='/api/image/resize/',
                data=payload,
                content_type='multipart/form-data'
            )

        self.task_id = str(response.json['task_id'])
        file = self.task_id + '.jpg'
        path = os.path.join(current_app.config['UPLOAD_DIR'], file)
        print(path)
        print(exists(path))

        self.assertEqual(202, response.status_code)
        self.assertIsNotNone(self.task_id)
        self.assertIsNotNone(response.json['task_id'])

        # os.remove(path)

    # def test_get_status_success(self):
    #     sleep(.5)
    #     with open(current_app.config['TEST_IMAGE'], 'rb') as fp:
    #         payload = {
    #             'image_data': fp,
    #             'size_target': '{"width": 100, "height": 100}'
    #         }
    #         post_response = self.client.post(
    #             path='/api/image/resize/',
    #             data=payload,
    #             content_type='multipart/form-data'
    #         )
    #         self.task_id2 = str(post_response.json['task_id'])
    #
    #     response = self.client.get(path=f'/api/image/task/{self.task_id2}')
    #
    #     file = f'{self.task_id2}.jpg'
    #     path = os.path.join(os.path.join(current_app.config['UPLOAD_DIR']), file)
    #     # os.remove(path)
    #
    #     print(response.json['status'])
    #     print(response.json['url_image'])
    #     self.assertEqual(response.status_code, 206)
    #     # self.assertEqual('SUCCESS', response.json['status'])
    #     self.assertEqual('PENDING', response.json['status'])
    #     # self.assertIsNotNone(response.json['url_image'])
    #     self.assertIsNone(response.json['url_image'])



