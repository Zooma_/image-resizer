onFileUpload (event) {
  this.form.file = event.target.files[0]
},
wait (delay) {
  return new Promise(resolve => setTimeout(() => resolve(), delay))
},
async onSubmit () {
  // post request
  const postResponse = await this.postImageResize()
  this.task_id = postResponse.data.task_id
  console.log('post response')
  console.log(postResponse)
  console.log('task_id: ' + this.task_id)
  // get request
  // const getResponse = await this.getUrl(this.task_id)
  const getResponse = await this.waitResponse(this.task_id)
  console.log('get response')
  console.log(getResponse)
  // url
  const resizedImageUrl = this.axios.defaults.baseURL + getResponse.data['url_image']
  console.log('url_image: ' + getResponse.data['url_image'])
  console.log(resizedImageUrl)
  const itemResult = {
    id: this.task_id,
    status: getResponse.status,
    url_image: resizedImageUrl
  }
  this.$emit('add-item', itemResult)
},
async waitResponse (token) {
  let response = await this.getUrl(token)
  if (response.status === 200) {
    console.log(response)
    return response
  } else if (response.status === 206) {
    console.log(response)
    console.log('wait 1 second')
    await this.wait(1000)
    console.log('try again')
    return this.waitResponse(token)
  }
},
async getUrl (token) {
  return this.axios.get('api/image/task/' + token)
},
postImageResize () {
  const formData = new FormData()
  const sizeTarget = JSON.stringify({width: this.form.widthTarget, height: this.form.widthTarget})
  formData.append('size_target', sizeTarget)
  formData.append('image_data', this.form.file)
  return this.axios.post('api/image/resize/', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    },
    onUploadProgress: function (progressEvent) {
      this.uploadPercentage = parseInt(Math.round(progressEvent.loaded / progressEvent.total) * 100)
    }.bind(this)
  })
}