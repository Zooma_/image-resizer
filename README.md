# Image resizer
Web service for resize images

### Installation

```bash
git clone git@gitlab.com:Zooma_/image-resizer.git
```

#### Build & Launch

```bash
docker-compose up -d --build
```

#### Stop
```
docker-compose down
```

#### Get status and logs
```
docker-compose ps
docker-compose logs
```
or
```
docker ps
docker logs CONTAINER_ID 
```

This will expose the Flask application's endpoints on port `5000` as well as a [Flower](https://github.com/mher/flower) server for monitoring workers on port `5555`

#### Scaling
To add more workers:
```bash
docker-compose up -d --scale worker=4 --no-recreate
```
This will create 4 more containers each running a worker. http://your-dockermachine-ip:5555 should now show 4 workers waiting for some jobs!

## API reference:
### Create resize task:
```
POST /api/image/resize/ HTTP/1.1
Content-Type: multipart/form-data
```
Accepts a file in multipart/form-data format and width and height parameters Returns a json with created task id, or error

REQUEST:
```
curl -X POST "http://localhost:5000/api/image/resize/?widthTarget=100&heightTarget=100" \
-H "accept: application/json" \
-H "Content-Type: multipart/form-data" \
-F "imageData=@/absolute/path/to/image/test.jpg; type=image/jpeg"
```


RESPONSE:
```
{
    "token": "7d656f51-a2fa-42cf-a66a-465955f83793"
}
```

### Get state task
Accepts a task id in UUID format as a GET parameter, and returns state of that task with url to resized image, or returns 404 if there is not task with given id
```
GET /api/image/task/<uuid:token> HTTP/1.1
```
REQUEST:
```
curl -X GET "http://localhost:5000/api/image/task/36d21e47-72fe-4542-983d-506f4d1f58d2" \
-H  "accept: application/json"
```

RESPONSE:
```
{
    "status": "SUCCESS",
    "resized_image_url": "images/7d656f51-a2fa-42cf-a66a-465955f83793.jpg"
}
```

### TO DO
* Update tests
* Add vue front
